package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity2 {
	
	WebDriver driver;
	WebDriverWait wait;
	
	@BeforeClass
	  public void beforeClass() {
		//Create a new instance of the Firefox driver
	      driver = new FirefoxDriver();
	      wait = new WebDriverWait(driver, 50);
	      Reporter.log("Starting Test |");
	      
	    //Open browser
	      driver.get("http://alchemy.hguy.co/crm");
	      Reporter.log("Opened Browser |");
	  }
  @Test
  public void f() {
	  
	 WebElement titleSuiteCRM = driver.findElement(By.xpath("/html/body/div[1]/div[1]/a")); 
	 
	 String title = titleSuiteCRM.getAttribute("href");
	 
	 System.out.println("Title of SuiteCRM:" + " " +title);
	 
	 Reporter.log("Title of SuiteCRM:" +title);
	 
  }
  

  @AfterClass
  public void afterClass() {
	  Reporter.log("Ending Test |");
      //Close the driver
      driver.close();
  }

}
