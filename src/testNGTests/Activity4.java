package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity4 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 10);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
  }
  
 /* @DataProvider(name = "Authentication")
  public static Object[][] credentials() {
      return new Object[][] { { "admin", "pa$$w0rd" }}; */
  
	  
	    
  @Test 
  public void loginTestCase()  {
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      WebElement dashboard = driver.findElement(By.xpath("//*[@id=\"tab0\"]"));
      String dash = dashboard.getText();         
      
      System.out.println("SuiteCRM Dashboard displays as:" + "" +dash);
      Reporter.log("Home screen displays:" + "" +dash);
  }
  
  @AfterClass
  public void afterClass() {
	  
	  Reporter.log("Ending Test |");
      //Close the driver
      driver.close();
  }

}
