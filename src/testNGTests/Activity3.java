package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity3 {
	
	WebDriver driver;
	WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	//Create a new instance of the Firefox driver
      driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      Reporter.log("Starting Test |");
      
    //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Opened Browser |");
	  }
  
  @Test
  public void f() {
	  
	  Reporter.log("Verify first copyright |");
	  WebElement firstCopyrightText = driver.findElement(By.id("admin_options"));
	  
	  String copyrightText = firstCopyrightText.getText();
	  
	  System.out.println("First CopyrightText is:" + " " +copyrightText);
	  
	  Reporter.log("First CopyrightText is:" + " " +copyrightText);
	  
  }
  

  @AfterClass
  public void afterClass() {
	  
	  Reporter.log("Ending Test |");
      //Close the driver
      driver.close();
  }

}
