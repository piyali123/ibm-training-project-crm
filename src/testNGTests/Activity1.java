package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;

public class Activity1 {
	
	WebDriver driver;
	WebDriverWait wait;
	
  @BeforeMethod
	  public void beforeMethod() {
	   
	 //Create a new instance of the Firefox driver
       driver = new FirefoxDriver();
       wait = new WebDriverWait(driver, 50);
       Reporter.log("Starting Test |");
       
     //Open browser
       driver.get("http://alchemy.hguy.co/crm");
       Reporter.log("Opened Browser |");
	  }
  @Test
  public void title() {
	// Check the title of the page
      String title = driver.getTitle();
          
      //Print the title of the page
      System.out.println("Page title is: " + title);
          
          //Assertion for page title
      Assert.assertEquals("SuiteCRM", title);
      Reporter.log("Page title is:" +title);
  }
  

  @AfterMethod
  public void afterMethod() {
	  
	  Reporter.log("Ending Test |");
      //Close the driver
      driver.close();
  }

}
