package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity9 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  }
  @Test
  public void f() throws InterruptedException {
	  
      Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Thread.sleep(2000);     
      
            
           
      WebElement sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
      WebElement lead = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Leads\"]"));
      
      Actions builder = new Actions(driver);
      
      Action clickLead = builder.moveToElement(sales).click(lead).release(lead).build();
      
      clickLead.perform();  
      
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div[1]/h2")));
      
      List<WebElement> nameLeadList = driver.findElements(By.xpath("//td[@field='name']/b/a"));
      for(WebElement name:nameLeadList) {
    	  System.out.println("Lead names are: " +name.getText());
      }
      
      List<WebElement> userLeadList = driver.findElements(By.xpath("//td[@field='assigned_user_name']/a"));
      for(WebElement user:userLeadList) {
    	  System.out.println("Lead names are: " +user.getText());
      }
  }
  

  @AfterClass
  public void afterClass() {
	  
	  Reporter.log("Ending Test |");
	  //Close the driver
	  driver.close();
  }

}
