package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity10 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  }
  
  @Test
  public void f() throws InterruptedException {
      Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Thread.sleep(2000);  
      
      List<WebElement> dashletList = driver.findElements(By.xpath("//li[starts-with(@id, 'dashlet_')]"));
      System.out.println("Total Dashlet Count :" + dashletList.size());
      
      List<WebElement> dashletTitleList = driver.findElements(By.xpath("//td[contains(@class, 'dashlet-title')]"));
      for(WebElement dashletTitle:dashletTitleList )
      {
    	  System.out.println("DashletTitle : " + dashletTitle.getText());
      }
      
  }
  

  @AfterClass
  public void afterClass() {
	  
	  Reporter.log("Ending Test |");
	  //Close the driver
	  driver.close();
  }

}
