package testNGTests;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Activity13 {
	WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor js = (JavascriptExecutor)driver;
	
  @BeforeClass
	  public void beforeClass() {
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  }
  @Test
  public void f() throws InterruptedException, IOException {
      Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Thread.sleep(3000);
      
      JavascriptExecutor js = (JavascriptExecutor) driver;
	  Actions builder = new Actions(driver);
		
	//Select All Menu
	  WebElement allMenu = driver.findElement(By.xpath("//a[@id='grouptab_5']"));
	  
	  builder.click(allMenu).build().perform();
	  
	  Thread.sleep(2000);
	 
	  WebElement dropDown = allMenu.findElement(By.xpath("//ul[@class='dropdown-menu']/li[25]/a"));
	  
	  js.executeScript("arguments[0].scrollIntoView(true);", dropDown);
	  
	  
	  
	  js.executeScript("arguments[0].click();", dropDown);
	  
	  
	  
	  
	  wait.until(ExpectedConditions.titleContains("Products"));
	  
	  Thread.sleep(2000);
	  
	//Read the file 
	  String filePath = "src/SuiteCRM/ProductFile2.xlsx";
	  
	  FileInputStream file = new FileInputStream(filePath);
	  
	  XSSFWorkbook workbook = new XSSFWorkbook(file);
	  
	  XSSFSheet sheet = workbook.getSheetAt(0);
	  
	  
	  
	  System.out.println("The total no of rows is" + sheet.getPhysicalNumberOfRows());
	  
	  int rowNum = sheet.getPhysicalNumberOfRows()-1;
	  
	  int colNum = sheet.getRow(0).getPhysicalNumberOfCells();
	  
	  System.out.println("The Row Size is"+rowNum);
	  System.out.println("The Column Size is"+colNum);
	  
	  String[][] data = new String[rowNum][colNum];
	  for (int i = 0;i<=rowNum-1;i++) {
		  
		  XSSFRow row = sheet.getRow(i);
		  
		    for(int j=0;j<colNum;j++) {
		    	
		    	XSSFCell cell = row.getCell(j);
		    	if(cell==null) {
		    		data[i][j] = "";
			       
		    	} else {
		    		String k = row.getCell(j).toString();
			       	data[i][j] = k;
			       
		    	}
		    	
		    	
		    	
		    }
		  
	  }
	    
	  System.out.println("The size of the array is" + data.length);
	  
	  
	  for(int l=1;l<=data.length-1;l++) {
	
		  
		 
		  WebElement productName = driver.findElement(By.xpath("//input[@id='name']"));
		  
		  productName.sendKeys(data[l][0]);
		  
		  
		  WebElement cost = driver.findElement(By.xpath("//input[@id='cost']"));
		  
		  cost.sendKeys(data[l][1]);
		  
		  
		  WebElement contact = driver.findElement(By.xpath("//input[@id='contact']"));
		  
		  contact.sendKeys(data[l][2]);
		  
		  
		  WebElement desc = driver.findElement(By.xpath("//textarea[@id='description']"));
		  
		  desc.sendKeys(data[l][3]);
		  
		  
		  WebElement partNo = driver.findElement(By.xpath("//input[@id='part_number']"));
		  
		  partNo.sendKeys(data[l][4]);
		  
		  
		  WebElement productTypeList = driver.findElement(By.xpath("//select[@id='type']"));
		  Select productType = new Select(productTypeList);
		  
		  
		  System.out.println("Data 1 5 is value is"+data[l][5]);
		  
		 int no = data[l][5].compareTo("Service");
		 if(no == 0) {
		 
			 productType.selectByVisibleText("Service");
			  
		  } 
		 
		  System.out.println("The selected value is" +productType.getFirstSelectedOption().getText());
		  
		  
		  WebElement price = driver.findElement(By.xpath("//input[@id='price']"));
		  
		  price.sendKeys(data[l][6]);
		  
		  
		  WebElement url = driver.findElement(By.xpath("//input[@id='url']"));
		  url.sendKeys(data[l][7]);
		  
		  
		  WebElement saveButton = driver.findElement(By.xpath("//input[@id='SAVE' and @title='Save']"));
		  saveButton.click();
		  
		 Thread.sleep(4000);
		
		
		 WebElement viewProd = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='View Products']"));
		  
		
		 js.executeScript("arguments[0].click()", viewProd);
		 
		
		 Thread.sleep(4000); 
				 
		 
		 WebElement createProd1 = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Create Product']"));		  
		
		 js.executeScript("arguments[0].click()", createProd1);
		 
		 		 
		 Thread.sleep(4000);
			
	  }
	

	  
	  

      
        }
  

  @AfterClass
  public void afterClass() {
	// Reporter.log("Ending Test |");
	//Close the driver
	// driver.close();
  }

}
