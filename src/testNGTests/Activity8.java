package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity8 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  
	  
	  }
  @Test
  public void f() throws InterruptedException {
	  
Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Thread.sleep(3000);    
      
      WebElement sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
      WebElement Accounts = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Accounts\"]"));
      
      Actions builder = new Actions(driver);
      Action clickAccount = builder.moveToElement(sales).click(Accounts).release(Accounts).build();
      
      clickAccount.perform();
      
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div[3]/form[2]/div[3]/table/thead/tr[1]/th[3]/div/a")));
     
      List<WebElement> nameAccounts = driver.findElements(By.xpath("//td[@field='name']/b/a")); 
      //List<WebElement> nameAccounts = driver.findElements(By.xpath("//td[@class='inlineEdit'])"));
      
     // for(WebElement name:nameAccounts)
    //	  System.out.println("Names:" +name.getText());
      
      System.out.println("First Five AccoutNames ");
      
      for(int count =0; count<5 ; count++)
      {
    	  System.out.println("Name " + (count+1) +  " : "  + nameAccounts.get(count).getText());
      }
      		
      
  }
  

  @AfterClass
  public void afterClass() {
	  Reporter.log("Ending Test |");
	  //Close the driver
	  driver.close();
  }

}
