package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;





public class Activity11 {
	
	WebDriver driver;
    WebDriverWait wait;
    File file = new File("C:\\Users\\PIYALIDAS\\eclipse-workspace\\Batch-2-CRM\\src\\testNGTests\\Activity11new.csv");
	
  @BeforeClass
	  public void beforeClass() {
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  }
	
  @Test
  public void f() throws InterruptedException  {
	  

      Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Thread.sleep(2000);          
            
           
      WebElement sales = driver.findElement(By.xpath("//*[@id=\"grouptab_0\"]"));
      WebElement lead = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Leads\"]"));
      
      Actions builder = new Actions(driver);
      
      Action clickLead = builder.moveToElement(sales).click(lead).release(lead).build();
      
      clickLead.perform();  
      
      //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div[1]/h2")));
      
      Thread.sleep(2000);
      
     // WebElement leads = driver.findElement(By.id("moduleTab_Leads"));
      
     // leads.click();
      
      WebElement importLeads = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Import Leads']"));
      
      importLeads.click();    
      
      WebElement uploadFile=driver.findElement(By.id("userfile"));
      uploadFile.sendKeys(file.getAbsolutePath());
      
      driver.findElement(By.id("gonext")).click();
      
      driver.findElement(By.id("gonext")).click();
      
      driver.findElement(By.id("gonext")).click();
      Thread.sleep(3000);
      
      driver.findElement(By.id("importnow")).click();
      Thread.sleep(3000);
      
      driver.findElement(By.id("finished")).click();
      Thread.sleep(2000);
      
      
      
     //Action clickImportLead = builder.moveToElement(leads).click(importLeads).release(importLeads).build().perform();
      
     //clickImportLead.perform();
		
  }


	  
  
  
  @AfterClass
  public void afterClass() {
	  Reporter.log("Ending Test |");
	  //Close the driver
	  driver.close();
  }

}
