package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity12 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 50);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  
	  }
  @Test
  public void f() throws InterruptedException {
      Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Thread.sleep(5000);
      
      WebElement activities = driver.findElement(By.xpath("//*[@id=\"grouptab_3\"]"));
      WebElement meetings = driver.findElement(By.xpath("//*[@id=\"moduleTab_9_Meetings\"]"));
      
      Actions builder = new Actions(driver);
      
      Action clickMeetings = builder.moveToElement(activities).click(meetings).release(meetings).build();
      
      clickMeetings.perform();
      
      Thread.sleep(5000);
      
      WebElement scheduleMeeting = driver.findElement(By.xpath("//div[@class='actionmenulink' and text()='Schedule Meeting']"));
      scheduleMeeting.click();
      
      Thread.sleep(5000);
      
      WebElement subject = driver.findElement(By.xpath("//*[@id=\"name\"]"));
      
      subject.sendKeys("Testing Meeting");
      
      driver.findElement(By.xpath("//*[@id=\"create_invitee_as_lead\"]")).click();
      
      Thread.sleep(5000);
      
      WebElement firstName = driver.findElement(By.name("first_name"));
      WebElement lastName = driver.findElement(By.name("last_name"));
      WebElement email = driver.findElement(By.name("email1"));
      
      firstName.sendKeys("testfirstname");
      lastName.sendKeys("testlastname");
      email.sendKeys("testing@gmail.com");
      
      driver.findElement(By.xpath("//*[@id=\"create-invitee-btn\"]")).click();
      
      Thread.sleep(3000);
      
      driver.findElement(By.xpath("//*[@id=\"create_invitee_as_lead\"]")).click();
      
      Thread.sleep(5000);
      
      WebElement firstNamesecond = driver.findElement(By.name("first_name"));
      WebElement lastNamesecond = driver.findElement(By.name("last_name"));
      WebElement emailsecond = driver.findElement(By.name("email1"));
      
      firstNamesecond.sendKeys("test2firstname");
      lastNamesecond.sendKeys("test2lastname");
      emailsecond.sendKeys("testing2@gmail.com");
      
      driver.findElement(By.xpath("//*[@id=\"create-invitee-btn\"]")).click();
      
      Thread.sleep(5000);
      
      driver.findElement(By.xpath("//*[@id=\"save_and_send_invites_header\"]")).click();
      
      Thread.sleep(5000);
      
      
  }
  

  @AfterClass
  public void afterClass() {
	 // Reporter.log("Ending Test |");
	  //Close the driver
	 // driver.close();
  }

}
