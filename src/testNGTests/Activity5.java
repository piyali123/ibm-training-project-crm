package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity5 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  driver = new FirefoxDriver();
      wait = new WebDriverWait(driver, 10);
      
      //Open browser
      driver.get("http://alchemy.hguy.co/crm");
      Reporter.log("Browser Opened |");
	  }
  @Test
  public void f() {
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      WebElement navBar = driver.findElement(By.cssSelector(".desktop-toolbar"));
      
      String color = navBar.getCssValue("color");
      
      Assert.assertEquals(color, "rgb(83, 77, 100)");
      
      System.out.println("Color of the navigation menu:" +color);
	  
  }
  

  @AfterClass
  public void afterClass() {
	  
	  Reporter.log("Ending Test |");
      //Close the driver
      driver.close();
  }

}
