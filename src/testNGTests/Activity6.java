package testNGTests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity6 {
	
	WebDriver driver;
    WebDriverWait wait;
	
  @BeforeClass
	  public void beforeClass() {
	  
		  driver = new FirefoxDriver();
	      wait = new WebDriverWait(driver, 10);
	      
	      //Open browser
	      driver.get("http://alchemy.hguy.co/crm");
	      Reporter.log("Browser Opened |");
	  }
  
  @Test
  public void f() {
	  
	  Reporter.log("Logging to the application with valid user id and password |");
	  
	  WebElement usernameField = driver.findElement(By.id("user_name"));
      WebElement passwordField = driver.findElement(By.id("username_password"));
      
      usernameField.sendKeys("admin");
      passwordField.sendKeys("pa$$w0rd");
      
            
      WebElement loginBtn = driver.findElement(By.id("bigbutton"));
      
      loginBtn.click();
      
      Reporter.log("Finding the navigation menu |");
      
      WebElement navBar = driver.findElement(By.cssSelector(".desktop-toolbar")); 
	 
	  System.out.println("Navigation Menu is displayed:" +navBar.isDisplayed());
	  
	  Reporter.log("Finding the Activities menu |");
	  
	  WebElement activityMenu = driver.findElement(By.xpath("//*[@id=\"grouptab_3\"]"));
	  System.out.println("Activities menu is displayed:" +activityMenu.isDisplayed());	  
	 
	  
  }  

  @AfterClass
  public void afterClass() {
	  
	  Reporter.log("Ending Test |");
      //Close the driver
      driver.close();
  }

}
